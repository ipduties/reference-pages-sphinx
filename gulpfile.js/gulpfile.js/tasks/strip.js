var browserSync   = require('browser-sync');
var config        = require('../util/loadConfig').stripcomments;
const { src, dest, series } = require('gulp');
const strip         = require('gulp-strip-comments');

function stripcomments(done) {
  browserSync.notify(config.notification);
  src(config.basedir+'/*.html')
    .pipe(strip())
    .pipe(dest(config.basedir+'/'));
    done();
};

exports.strip = series( stripcomments );
