Links
=====

.. Konsolidierte EU-Rechtsakte, insb. Kap 17.20 Geistiges Eigentumsrecht
`EUR-Lex`_, der Zugang zum EU-Recht (siehe insb. Kap. `17.20`_)

.. _EUR-Lex: https://eur-lex.europa.eu/browse/directories/consleg.html?locale=de

.. _17.20: https://eur-lex.europa.eu/search.html?displayProfile=lastConsDocProfile&qid=1583364846641&type=named&CC_1_CODED=17&name=browse-by:consleg-in-force&CC_2_CODED=1720

`EU (Parlament)`_, `EU (alle)`_

.. _EU (Parlament): https://europa.eu/european-union/about-eu/institutions-bodies/european-parliament_de

.. _EU (alle): https://europa.eu/european-union/contact/institutions-bodies_de

.. Richtlinie 98/44/EG des Europaischen Parlaments und des Rates vom 6. Juli 1998 Ober den rechtlichen Schutz biotechno- logischer Erfindungen zu EPÜ Tell 2, Art. 53, Regel 26
`Richtl. 98/44`_ über Biotech-Erfindungen

.. _Richtl. 98/44: https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:31998L0044&from=DE##

.. Verordnung 469/2009 Ober das ergänzende Schutzzertifikat fur Arzneimittel zu EPÜ Teil 2, Art. 63
`Verord. 469/2009`_ (ergänzendes Arzneimittel-Schutzzertifikat)

.. _Verord. 469/2009: https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2009:152:0001:0010:DE:PDF

Link-Checker
^^^^^^^^^^^^

`W3C-Linkchecker`_

.. _W3C-Linkchecker: https://validator.w3.org/checklink